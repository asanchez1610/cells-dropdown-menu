import {
  html, fixture, assert, fixtureCleanup,
} from '@open-wc/testing';
import '../cells-dropdown-menu.js';

suite('CellsDropdownMenu', () => {
  let el;

  teardown(() => fixtureCleanup());

  setup(async () => {
    el = await fixture(html`<cells-dropdown-menu></cells-dropdown-menu>`);
    const options = [
      {
        title: 'Enviar',
        name: 'send',
        color: '#000'
      },
      {
        title: 'Eliminar',
        name: 'trash'
      }
    ];

    el.options = options;
    await el.updateComplete;
  });

  test('Instantiating component', () => {
    const element = el.shadowRoot.querySelector('.content-menu');
    assert.isNotNull(element);
  });

  test('Click Window', () => {
    const click = new CustomEvent('click', { 
      detail: true,
      bubbles: true, 
      composed: true
    });
    window.dispatchEvent(click);
  });

  test('Test function show(visible)', () => {
    el.show(10, 10);
  });

  test('Test function show(hidden)', () => {
    el.shadowRoot.querySelector('.content-menu').classList.add('show');
    el.show(10, 10);
  });

  test('Test function selectedOption', () => {
    el.selectedOption({});
  });

  test('Test onClickSelection', () => {
    let li = el.shadowRoot.querySelector('li');
    const click = new CustomEvent('click', { 
      detail: true,
      bubbles: true, 
      composed: true
    });
    
    li.dispatchEvent(click);
  });

});
