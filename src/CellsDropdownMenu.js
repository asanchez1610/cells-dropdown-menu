import { html } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import { BaseElement } from '@bbva-commons-web-components/cells-base-elements/BaseElement.js';
import styles from './CellsDropdownMenu-styles.js';
import '@cells-components/coronita-icons/coronita-icons.js';
import '@cells-components/cells-icon/cells-icon.js';
/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<cells-dropdown-menu></cells-dropdown-menu>
```

##styling-doc

@customElement cells-dropdown-menu
*/
export class CellsDropdownMenu extends BaseElement {
  static get is() {
    return 'cells-dropdown-menu';
  }

  // Declare properties
  static get properties() {
    return {
      options: Array,
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.options = [];
    this.initComponent();
  }

  async initComponent() {
    await this.updateComplete;
    window.addEventListener('click', (e) => {
      e.stopPropagation();
      this.element('.content-menu').classList.remove('show');
    });
  }

  show(x, y) {
    if (this.element('.content-menu').classList.contains('show')) {
      this.element('.content-menu').classList.remove('show');
    } else {
      this.element('.content-menu').style.top = `${y}px`;
      this.element('.content-menu').style.left = `${x}px`;
      this.element('.content-menu').classList.add('show');
    }
  }

  static get styles() {
    return [
      styles,
      getComponentSharedStyles('cells-dropdown-menu-shared-styles'),
    ];
  }

  selectedOption(option) {
    this.dispatch('on-selection-option', option);
  }

  // Define a template
  render() {
    return html`
      <slot></slot>
      <div class="content-menu">
        <ul>
          ${this.options.map(item => html`
          <li @click="${() => this.selectedOption(item)}" >
            <cells-icon ?hidden="${!item.icon}" style="color:${item.color ? item.color : 'inherit'}" icon="${item.icon}" ></cells-icon> 
            ${item.title}
          </li> `)}
        </ul>
      </div>
    `;
  }
}
