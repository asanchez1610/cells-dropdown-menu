import { setDocumentCustomStyles, } from '@bbva-web-components/bbva-core-lit-helpers';
import { css, } from 'lit-element';

setDocumentCustomStyles(css`
  #iframeBody {
    margin: 0;
  }
  .container {
    margin-top: 30px;
    display: flex;
    align-items: flex-start;
    justify-content: center;
  }
`);
